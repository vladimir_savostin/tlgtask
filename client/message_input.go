package client

type tdCommon struct {
	Type  string `json:"@type"`
	Extra string `json:"@extra"`
}

// InputMessageText - text message
type InputMessageText struct {
	tdCommon
	Text                  *FormattedText `json:"text"`                     // Formatted text to be sent; 1-GetOption("message_text_length_max") characters. Only Bold, Italic, Code, Pre, PreCode and TextUrl entities are allowed to be specified manually
	DisableWebPagePreview bool           `json:"disable_web_page_preview"` // True, if rich web page previews for URLs in the message text should be disabled
	ClearDraft            bool           `json:"clear_draft"`              // True, if a chat message draft should be deleted
}

// FormattedText - text with some entities
type FormattedText struct {
	tdCommon
	Text     string       `json:"text"`     // The text
	Entities []TextEntity `json:"entities"` // Entities contained in the text
}

// TextEntity Represents a part of the text that needs to be formatted in some unusual way
type TextEntity struct {
	tdCommon
	Offset int32       `json:"offset"` // Offset of the entity in UTF-16 code points
	Length int32       `json:"length"` // Length of the entity, in UTF-16 code points
	Type   interface{} `json:"type"`   // Type of the entity
}
