package client

import (
	"fmt"
	"github.com/L11R/go-tdjson"
	"github.com/astaxie/beego/logs"
)

//Client - wrapper for tdjson.Client
type Client struct {
	tdClient *tdjson.Client
}

func NewClient(settings Settings) (*Client, error) {
	var params []tdjson.Option
	params = append([]tdjson.Option{
		tdjson.WithMessageDatabase(),
		tdjson.WithStorageOptimizer(),
	})
	c := new(Client)
	apiId := settings.ApiID
	if apiId == "" {
		return nil, fmt.Errorf("API_ID env variable not specified")
	}
	params = append(params, tdjson.WithID(apiId))

	apiHash := settings.ApiHash
	if apiHash == "" {
		return nil, fmt.Errorf("API_HASH env variable not specified")
	}
	params = append(params, tdjson.WithHash(apiHash))
	// Create new instance of client
	c.tdClient = tdjson.NewClient(params...)
	return c, nil
}

//ListenUpdates - слушает апдейты из телеграм и предпринимает свои варианты действий
func (c *Client) ListenUpdates() {
	for update := range c.tdClient.Updates {

		logs.Debug(update)
		updateType := update["@type"].(string)
		switch updateType {
		case typeUpdateAuthSate:
			if authState, ok := update["authorization_state"].(tdjson.Update)["@type"].(string); ok {
				logs.Warn("new auth state: %s", authState)
				res, err := c.tdClient.Auth(authState)
				if err != nil {
					logs.Error(err)
				}
				logs.Info(res)
			}
		case typeUpdateChatLastMessage:
			logs.Debug("got new message")
			//проверяем что это не исходящее сообщение, чтобы не отвечать самим себе
			if msg, ok := update["last_message"]; ok {
				if isOutGoing, ok := msg.(tdjson.Update)["is_outgoing"]; ok {
					if isOutGoing.(bool) {
						logs.Debug("do not replay for outgoing messages")
						continue
					}
				}
			}
			//проверяем, что это не групповой чат, группы начинаются со знака минус, их игнорируем
			chatID := update["chat_id"].(float64)
			if chatID < 0 {
				logs.Debug("do not replay to groups")
				continue
			}
			//todo do concurrent
			err := c.AnswerLastMsg(update)
			if err != nil {
				logs.Error(err)
			}
		}
	}
}

//Destroy - wrapper для c.tdClient.Destroy()
func (c *Client) Destroy() {
	logs.Info("destroy client")
	c.tdClient.Destroy()
}

//Send - wrapper для c.tdClient.Send(data)
func (c *Client) Send(data tdjson.Update) {
	c.tdClient.Send(data)
}

//SendMsgText - отправить текстовое сообщение text в чат chatId
func (c *Client) SendMsgText(chatId float64, text string) (resp tdjson.Update, err error) {
	data := make(tdjson.Update)
	data["@type"] = typeSendMessage
	data["chat_id"] = chatId
	data["reply_to_message_id"] = 0
	data["disable_notification"] = false
	data["from_background"] = true
	data["reply_markup"] = nil
	data["input_message_content"] = InputMessageText{
		tdCommon: tdCommon{Type: typeInputMessageText},
		Text: &FormattedText{
			tdCommon: tdCommon{Type: typeFormattedText},
			Text:     text,
			Entities: nil,
		},
		DisableWebPagePreview: false,
		ClearDraft:            true,
	}
	resp, err = c.tdClient.SendAndCatch(data)
	return resp, err
}

//AnswerLastMsg - отвечает на сообщение которое содержится в update,
//добавляя произвольный текст к исходному сообщению из update
func (c *Client) AnswerLastMsg(update tdjson.Update) error {
	//todo parse update to struct
	if msg, ok := update["last_message"]; ok {
		chatID := update["chat_id"].(float64)
		if content, ok := msg.(tdjson.Update)["content"]; ok {
			if data, ok := content.(tdjson.Update)["text"]; ok {
				if text, ok := data.(tdjson.Update)["text"]; ok {
					resp, err := c.SendMsgText(chatID, text.(string)+"\nСпасибо что написали, я это ценю!")
					logs.Debug(resp)
					return err
				}
			}
		}
	}
	return fmt.Errorf("there any field missed in this update: %v", update)
}
