package client

const (
	typeUpdateAuthSate        = "updateAuthorizationState"
	typeUpdateChatLastMessage = "updateChatLastMessage"
	typeSendMessage           = "sendMessage"
	typeInputMessageText      = "inputMessageText"
	typeFormattedText         = "formattedText"
)
