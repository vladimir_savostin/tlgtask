package client

//Settings - общие настройки для клиента
type Settings struct {
	ApiID   string `json:"api_id"`
	ApiHash string `json:"api_hash"`
}
