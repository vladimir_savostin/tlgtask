package main

import (
	"bitbucket.org/vladimir_savostin/tlgTask/client"
	"github.com/L11R/go-tdjson"
	"github.com/astaxie/beego/logs"
	"github.com/tkanos/gonfig"
	"os"
	"os/signal"
	"syscall"
)

const testChatID = 780317300

func main() {
	logs.SetLevel(logs.LevelDebug)
	logs.SetLogFuncCall(true)
	logs.SetLogFuncCallDepth(3)
	const settingsPath = "settings.json"
	tdjson.SetLogVerbosityLevel(1)
	tdjson.SetFilePath("./errors.txt")

	var settings client.Settings
	err := gonfig.GetConf(settingsPath, &settings)
	if err != nil {
		logs.Error(err)
		os.Exit(1)
	}

	service, err := client.NewClient(settings)
	if err != nil {
		logs.Error(err)
		os.Exit(1)
	}
	defer service.Destroy()

	go service.ListenUpdates()
	//logs.Warn("send test msg")
	//for i := 0; i <= 10; i++ {
	//	service.SendMsgText(testChatID, "this is test message!!!")
	//	time.Sleep(time.Second * 5)
	//}
	// Handle Ctrl+C
	ch := make(chan os.Signal, 2)
	signal.Notify(ch, os.Interrupt, syscall.SIGTERM)
	<-ch
	return
}
